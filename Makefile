test: cs phpstan phpunit

cbf:
	php vendor/bin/phpcbf -s --standard=ruleset.xml

cs:
	php vendor/bin/phpcs -s --standard=ruleset.xml

phpstan:
	php vendor/bin/phpstan

phpunit:
	php vendor/bin/phpunit tests
