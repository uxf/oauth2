# UXF OAuth2

### Minimal config
```yaml
uxf_oauth2:
    public_key_path: '%kernel.project_dir%/config/oauth2/public.key'
    private_key_path: '%kernel.project_dir%/config/oauth2/private.key'
    encryption_key: '<secured-key>'
    default_client:
        secret: '<your-client-secret>'
```

### Full config
```yaml
uxf_oauth2:
    authorization_header: 'Authorization' # default
    public_key_path: '%kernel.project_dir%/config/oauth2/public.key' # required
    private_key_path: '%kernel.project_dir%/config/oauth2/private.key' # required
    encryption_key: '<secured-key>' # required
    auth_code_lifetime: 'PT10M' # default
    access_token_lifetime: 'P1D' # default
    refresh_token_lifetime: 'P1M' # default
    default_scope: '' # default
    cookie_name: 'Auth-Token' # default
    cookie_secured: ~ # default
    cookie_http_only: true # default
    redirect_login_path: '/login' # default
    redirect_approve_path: '/approve' # default
    clients_whitelist: [] # default (fill client identifiers)
    default_client:
        id: 'uxf' # default
        secret: '<your-client-secret>' # required
        scope: 'uxf_scope' # default
```
