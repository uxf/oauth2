<?php

declare(strict_types=1);

namespace UXF\OAuth2\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('uxf_oauth2');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->scalarNode('authorization_header')->defaultValue('Authorization')->end()
            ->scalarNode('public_key_path')->isRequired()->end()
            ->scalarNode('private_key_path')->isRequired()->end()
            ->scalarNode('encryption_key')->isRequired()->end()
            ->scalarNode('auth_code_lifetime')->defaultValue('PT10M')->end()
            ->scalarNode('access_token_lifetime')->defaultValue('P1D')->end()
            ->scalarNode('refresh_token_lifetime')->defaultValue('P1M')->end()
            ->scalarNode('default_scope')->defaultValue('')->end()
            ->scalarNode('cookie_name')->defaultValue('Auth-Token')->end()
            ->scalarNode('cookie_secured')->defaultValue(null)->end()
            ->scalarNode('cookie_http_only')->defaultValue(true)->end()
            ->scalarNode('redirect_login_path')->defaultValue('/login')->end()
            ->scalarNode('redirect_approve_path')->defaultValue('/approve')->end()
            ->arrayNode('clients_whitelist')
                ->scalarPrototype()->defaultValue([])->end()
            ->end()
            ->arrayNode('default_client')
                ->children()
                    ->scalarNode('id')->defaultValue('uxf')->end()
                    ->scalarNode('secret')->isRequired()->end()
                    ->scalarNode('scope')->defaultValue('uxf_scope')->end()
                ->end()
            ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
