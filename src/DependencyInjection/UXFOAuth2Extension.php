<?php

declare(strict_types=1);

namespace UXF\OAuth2\DependencyInjection;

use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\ResourceServer;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use UXF\OAuth2\Security\Config\AuthorizationServerConfig;
use UXF\OAuth2\Security\Config\DefaultClientConfig;
use UXF\OAuth2\Security\UxfAuthenticator;

/**
 * @author Jakub Janata <jakubjanata@gmail.com>
 */
class UXFOAuth2Extension extends Extension
{
    /**
     * @param mixed[] $configs
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        $container->getDefinition(AuthorizationServerConfig::class)
            ->setArguments([
                '$redirectLoginPath' => $config['redirect_login_path'],
                '$redirectApprovePath' => $config['redirect_approve_path'],
                '$authCodeLifetime' => $config['auth_code_lifetime'],
                '$accessTokenLifetime' => $config['access_token_lifetime'],
                '$refreshTokenLifetime' => $config['refresh_token_lifetime'],
                '$defaultScope' => $config['default_scope'],
                '$clientsWhitelist' => $config['clients_whitelist'],
                '$cookieName' => $config['cookie_name'],
                '$cookieSecured' => $config['cookie_secured'],
                '$cookieHttpOnly' => $config['cookie_http_only'],
            ]);

        $container->getDefinition(UxfAuthenticator::class)
            ->setArguments([
                '$authorizationHeader' => $config['authorization_header']
            ]);

        $container->getDefinition(DefaultClientConfig::class)
            ->setArguments([
                '$clientId' => $config['default_client']['id'],
                '$clientSecret' => $config['default_client']['secret'],
                '$scope' => $config['default_client']['scope'],
            ]);

        $container->getDefinition(ResourceServer::class)
            ->setArguments(['$publicKey' => $config['public_key_path']]);

        $container->getDefinition(AuthorizationServer::class)
            ->setArguments([
                '$privateKey' => $config['private_key_path'],
                '$encryptionKey' => $config['encryption_key'],
            ]);
    }

    public function getAlias(): string
    {
        return 'uxf_oauth2';
    }
}
