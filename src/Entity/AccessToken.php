<?php

declare(strict_types=1);

namespace UXF\OAuth2\Entity;

use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\Traits\AccessTokenTrait;
use RuntimeException;

/**
 * @ORM\Entity
 * @ORM\Table(name="oauth_access_token", schema="uxf_oauth2")
 */
class AccessToken implements AccessTokenEntityInterface
{
    use AccessTokenTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $identifier;

    /**
     * @ORM\ManyToOne(targetEntity="UXF\OAuth2\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * @var Client
     */
    private $client;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $userIdentifier;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $expiryDateTime;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $revoked;

    /**
     * @ORM\ManyToMany(targetEntity="UXF\OAuth2\Entity\Scope", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *      name="oauth_access_token_scope",
     *      schema="uxf_oauth2",
     *      joinColumns={@ORM\JoinColumn(name="access_token_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="scope_id", referencedColumnName="id")}
     * )
     * @var Scope[]|Collection<int, Scope>
     */
    private $scopes;

    /**
     * @param Scope[] $scopes
     */
    public function __construct(Client $client, array $scopes, string $userIdentifier)
    {
        $this->client = $client;
        $this->scopes = new ArrayCollection($scopes);
        $this->userIdentifier = $userIdentifier;
        $this->revoked = false;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier): self
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(ClientEntityInterface $client): void
    {
        if (!$client instanceof Client) {
            throw new RuntimeException('$client must be instance of ' . Client::class);
        }
        $this->client = $client;
    }

    public function getExpiryDateTime(): DateTimeImmutable
    {
        return DateTimeImmutable::createFromMutable($this->expiryDateTime);
    }

    public function setExpiryDateTime(DateTimeImmutable $expiryDateTime): self
    {
        $this->expiryDateTime = DateTime::createFromImmutable($expiryDateTime);
        return $this;
    }

    public function isRevoked(): bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): self
    {
        $this->revoked = $revoked;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getScopes(): array
    {
        return $this->scopes->toArray();
    }

    public function addScope(ScopeEntityInterface $scope): void
    {
        if (!$scope instanceof Scope) {
            throw new RuntimeException('$scope must be instance of ' . Scope::class);
        }
        $this->scopes->add($scope);
    }

    public function getUserIdentifier(): string
    {
        return $this->userIdentifier;
    }

    /**
     * @param string $userIdentifier
     */
    public function setUserIdentifier($userIdentifier): self
    {
        $this->userIdentifier = $userIdentifier;
        return $this;
    }
}
