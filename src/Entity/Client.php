<?php

declare(strict_types=1);

namespace UXF\OAuth2\Entity;

use Doctrine\ORM\Mapping as ORM;
use League\OAuth2\Server\Entities\ClientEntityInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="oauth_client", schema="uxf_oauth2")
 */
class Client implements ClientEntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $identifier;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $secret;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private $redirectUri;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    public function __construct(string $identifier, string $secret, string $name, ?string $redirectUri = null)
    {
        $this->identifier = $identifier;
        $this->secret = $secret;
        $this->redirectUri = $redirectUri;
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function getRedirectUri(): ?string
    {
        return $this->redirectUri;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isConfidential(): bool
    {
        return true;
    }
}
