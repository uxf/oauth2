<?php

declare(strict_types=1);

namespace UXF\OAuth2\Entity;

use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="oauth_refresh_token", schema="uxf_oauth2")
 */
class RefreshToken implements RefreshTokenEntityInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $identifier;

    /**
     * @ORM\OneToOne(targetEntity="UXF\OAuth2\Entity\AccessToken")
     * @ORM\JoinColumn(name="access_token_id", referencedColumnName="id")
     * @var AccessTokenEntityInterface
     */
    private $accessToken;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $expiryDateTime;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $revoked;

    public function __construct()
    {
        $this->revoked = false;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier): self
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getExpiryDateTime(): DateTimeImmutable
    {
        return DateTimeImmutable::createFromMutable($this->expiryDateTime);
    }

    public function setExpiryDateTime(DateTimeImmutable $expiryDateTime): self
    {
        $this->expiryDateTime = DateTime::createFromImmutable($expiryDateTime);
        return $this;
    }
    public function getAccessToken(): AccessTokenEntityInterface
    {
        return $this->accessToken;
    }

    public function setAccessToken(AccessTokenEntityInterface $accessToken): self
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    public function isRevoked(): bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): self
    {
        $this->revoked = $revoked;
        return $this;
    }
}
