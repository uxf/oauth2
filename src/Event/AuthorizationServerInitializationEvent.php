<?php

declare(strict_types=1);

namespace UXF\OAuth2\Event;

use League\OAuth2\Server\AuthorizationServer;
use Symfony\Contracts\EventDispatcher\Event;

class AuthorizationServerInitializationEvent extends Event
{
    /** @var AuthorizationServer */
    private $authorizationServer;

    public function __construct(AuthorizationServer $authorizationServer)
    {
        $this->authorizationServer = $authorizationServer;
    }

    public function getAuthorizationServer(): AuthorizationServer
    {
        return $this->authorizationServer;
    }
}
