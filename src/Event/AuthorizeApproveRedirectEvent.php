<?php

declare(strict_types=1);

namespace UXF\OAuth2\Event;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Contracts\EventDispatcher\Event;

class AuthorizeApproveRedirectEvent extends Event
{
    /** @var ServerRequestInterface */
    private $request;

    /** @var ResponseInterface|null */
    private $response;

    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }

    public function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }

    public function setResponse(ResponseInterface $response): void
    {
        $this->response = $response;
    }
}
