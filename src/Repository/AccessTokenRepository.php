<?php

declare(strict_types=1);

namespace UXF\OAuth2\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use RuntimeException;
use UXF\OAuth2\Entity\AccessToken;
use UXF\OAuth2\Entity\Client;
use UXF\OAuth2\Entity\Scope;

/**
 * @extends ServiceEntityRepository<AccessToken>
 * @method AccessToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccessToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccessToken[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method AccessToken[] findAll()
 */
class AccessTokenRepository extends ServiceEntityRepository implements AccessTokenRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccessToken::class);
    }

    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity): void
    {
        $accessToken = $this->findOneBy(['identifier' => $accessTokenEntity->getIdentifier()]);

        if ($accessToken !== null) {
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }

        // TODO: clean expired tokens

        $this->getEntityManager()->persist($accessTokenEntity);
        $this->getEntityManager()->flush();
    }

    public function revokeAccessToken($tokenId): void
    {
        $token = $this->findOneBy(['identifier' => $tokenId]);

        if ($token === null) {
            return;
        }

        $token->setRevoked(true);

        $this->getEntityManager()->flush();
    }

    public function isAccessTokenRevoked($tokenId): bool
    {
        $token = $this->findOneBy(['identifier' => $tokenId]);

        return $token === null ? true : $token->isRevoked();
    }

    public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null): AccessToken
    {
        if (!$clientEntity instanceof Client) {
            throw new RuntimeException('$client must be instance of ' . Client::class);
        }

        foreach ($scopes as $scope) {
            if (!$scope instanceof Scope) {
                throw new RuntimeException('$scopes must be instance of ' . Scope::class . '[]');
            }
        }

        /** @var Scope[] $scopes */
        return new AccessToken($clientEntity, $scopes, (string) $userIdentifier);
    }
}
