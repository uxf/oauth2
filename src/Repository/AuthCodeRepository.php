<?php

declare(strict_types=1);

namespace UXF\OAuth2\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use UXF\OAuth2\Entity\AuthCode;

/**
 * @extends ServiceEntityRepository<AuthCode>
 * @method AuthCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthCode[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method AuthCode[] findAll()
 */
class AuthCodeRepository extends ServiceEntityRepository implements AuthCodeRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AuthCode::class);
    }

    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity): void
    {
        $this->getEntityManager()->persist($authCodeEntity);
        $this->getEntityManager()->flush();
    }

    public function revokeAuthCode($codeId): void
    {
        $authCode = $this->findOneBy(['identifier' => $codeId]);

        if ($authCode === null) {
            return;
        }

        $authCode->setRevoked(true);

        $this->getEntityManager()->flush();
    }

    public function isAuthCodeRevoked($codeId): bool
    {
        $authCode = $this->findOneBy(['identifier' => $codeId]);

        return $authCode === null ? true : $authCode->isRevoked();
    }

    public function getNewAuthCode(): AuthCode
    {
        return new AuthCode();
    }
}
