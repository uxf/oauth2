<?php

declare(strict_types=1);

namespace UXF\OAuth2\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use UXF\OAuth2\Entity\Client;

/**
 * @extends ServiceEntityRepository<Client>
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Client[] findAll()
 */
class ClientRepository extends ServiceEntityRepository implements ClientRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function getClientEntity($clientIdentifier): ?Client
    {
        return $this->findOneBy(['identifier' => $clientIdentifier]);
    }

    /**
     * Validate a client's secret.
     *
     * @param string $clientIdentifier The client's identifier
     * @param null|string $clientSecret The client's secret (if sent)
     * @param null|string $grantType The type of grant the client is using (if sent)
     *
     * @return bool
     */
    public function validateClient($clientIdentifier, $clientSecret, $grantType): bool
    {
        $client = $this->getClientEntity($clientIdentifier);

        return
            $client !== null
            && $clientSecret !== null
            && password_verify($clientSecret, $client->getSecret());
    }
}
