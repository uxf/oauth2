<?php

declare(strict_types=1);

namespace UXF\OAuth2\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use UXF\OAuth2\Entity\RefreshToken;

/**
 * @extends ServiceEntityRepository<RefreshToken>
 * @method RefreshToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method RefreshToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method RefreshToken[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method RefreshToken[] findAll()
 */
class RefreshTokenRepository extends ServiceEntityRepository implements RefreshTokenRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RefreshToken::class);
    }

    public function getNewRefreshToken(): RefreshToken
    {
        return new RefreshToken();
    }

    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntityInterface): void
    {
        $refreshToken = $this->findOneBy(['identifier' => $refreshTokenEntityInterface->getIdentifier()]);

        if ($refreshToken !== null) {
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }

        // TODO: remove expired tokens

        $this->getEntityManager()->persist($refreshTokenEntityInterface);
        $this->getEntityManager()->flush();
    }

    public function revokeRefreshToken($tokenId): void
    {
        $token = $this->findOneBy(['identifier' => $tokenId]);

        if ($token === null) {
            return;
        }

        $token->setRevoked(true);

        $this->getEntityManager()->flush();
    }

    public function isRefreshTokenRevoked($tokenId): bool
    {
        $token = $this->findOneBy(['identifier' => $tokenId]);

        if ($token === null) {
            return true;
        }

        return $token->isRevoked();
    }
}
