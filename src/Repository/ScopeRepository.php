<?php

declare(strict_types=1);

namespace UXF\OAuth2\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use UXF\OAuth2\Entity\Scope;

/**
 * @extends ServiceEntityRepository<Scope>
 * @method Scope|null findOneBy(array $criteria, array $orderBy = null)
 * @method Scope|null find($id, $lockMode = null, $lockVersion = null)
 * @method Scope[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Scope[] findAll()
 */
class ScopeRepository extends ServiceEntityRepository implements ScopeRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Scope::class);
    }

    public function getScopeEntityByIdentifier($identifier): ?Scope
    {
        return $this->findOneBy(['identifier' => $identifier]);
    }

    /**
     * @inheritDoc
     */
    public function finalizeScopes(
        array $scopes,
        $grantType,
        ClientEntityInterface $clientEntity,
        $userIdentifier = null
    ): array {
        return $scopes;
    }
}
