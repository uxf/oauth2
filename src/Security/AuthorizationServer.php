<?php

declare(strict_types=1);

namespace UXF\OAuth2\Security;

use League\OAuth2\Server\AuthorizationServer as LeagueAuthorizationServer;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AuthCodeGrant;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Nette\Utils\Json;
use Nyholm\Psr7\Response;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use UXF\OAuth2\Event\AuthorizationServerInitializationEvent;
use UXF\OAuth2\Event\AuthorizeApproveRedirectEvent;
use UXF\OAuth2\Event\AuthorizeLoginRedirectEvent;
use UXF\OAuth2\Security\Config\AuthorizationServerConfig;
use UXF\OAuth2\Security\Config\DefaultClientConfig;
use UXF\OAuth2\Utils\OAuthHelper;

class AuthorizationServer
{
    /** @var LeagueAuthorizationServer */
    private $server;

    /** @var UserRepositoryInterface */
    private $userRepository;

    /** @var AuthCodeRepositoryInterface */
    private $authCodeRepository;

    /** @var RefreshTokenRepositoryInterface */
    private $refreshTokenRepository;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var LoggerInterface */
    private $logger;

    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var AuthorizationServerConfig */
    private $serverConfig;

    /** @var DefaultClientConfig */
    private $defaultClientConfig;

    /** @var bool */
    private $initialized = false;

    public function __construct(
        LeagueAuthorizationServer $server,
        UserRepositoryInterface $userRepository,
        AuthCodeRepositoryInterface $authCodeRepository,
        RefreshTokenRepositoryInterface $refreshTokenRepository,
        TokenStorageInterface $tokenStorage,
        LoggerInterface $logger,
        EventDispatcherInterface $dispatcher,
        AuthorizationServerConfig $serverConfig,
        DefaultClientConfig $defaultClientConfig
    ) {
        $this->server = $server;
        $this->userRepository = $userRepository;
        $this->authCodeRepository = $authCodeRepository;
        $this->refreshTokenRepository = $refreshTokenRepository;
        $this->tokenStorage = $tokenStorage;
        $this->logger = $logger;
        $this->dispatcher = $dispatcher;
        $this->serverConfig = $serverConfig;
        $this->defaultClientConfig = $defaultClientConfig;
    }

    public function authorize(ServerRequestInterface $request): ResponseInterface
    {
        $this->initialize();
        $response = new Response();

        try {
            // Validate the HTTP request and return an AuthorizationRequest object.
            $authRequest = $this->server->validateAuthorizationRequest($request);

            $token = $this->tokenStorage->getToken();

            if ($token === null || !$token->getUser() instanceof UserEntityInterface) {
                // redirect to login
                $event = new AuthorizeLoginRedirectEvent($request);
                $this->dispatcher->dispatch($event);

                if ($event->getResponse() !== null) {
                    return $event->getResponse();
                }

                return $response
                    ->withHeader('Location', $this->serverConfig->getRedirectLoginPath())
                    ->withStatus(302);
            }

            $whitelist = $this->serverConfig->getClientsWhitelist();
            if (!in_array($authRequest->getClient()->getIdentifier(), $whitelist, true)) {
                // redirect to approve page
                $event = new AuthorizeApproveRedirectEvent($request);
                $this->dispatcher->dispatch($event);

                if ($event->getResponse() !== null) {
                    return $event->getResponse();
                }

                return $response
                    ->withHeader('Location', $this->serverConfig->getRedirectApprovePath())
                    ->withStatus(302);
            }

            $authRequest->setUser($token->getUser());
            $authRequest->setAuthorizationApproved(true);

            return $this->server->completeAuthorizationRequest($authRequest, $response);
        } catch (OAuthServerException $e) {
            return $e->generateHttpResponse($response);
        }
    }

    public function token(ServerRequestInterface $request): ResponseInterface
    {
        $this->initialize();

        if ($request->getHeaderLine('Content-Type') !== 'application/x-www-form-urlencoded') {
            // convert body to parsed body
            $request = $request->withParsedBody(Json::decode((string) $request->getBody(), Json::FORCE_ARRAY));
        }

        $response = new Response();

        try {
            return $this->server->respondToAccessTokenRequest($request, $response);
        } catch (OAuthServerException $e) {
            if ($e->getHttpStatusCode() !== 401) {
                $this->logger->critical($e->getMessage(), $e->getTrace());
            }

            return $e->generateHttpResponse($response);
        }
    }

    public function login(string $username, string $password, ServerRequestInterface $request): ResponseInterface
    {
        $this->initialize();

        $request = $request
            ->withParsedBody([
                'grant_type' => 'password',
                'username' => $username,
                'password' => $password,
                'client_id' => $this->defaultClientConfig->getClientId(),
                'client_secret' => $this->defaultClientConfig->getClientSecret(),
                'scope' => $this->defaultClientConfig->getScope(),
            ]);

        $response = new Response();

        try {
            $response = $this->server->respondToAccessTokenRequest($request, $response);
            return OAuthHelper::saveTokenToCookie(
                $response,
                $this->serverConfig->getCookieName(),
                $this->serverConfig->isCookieSecured(),
                $this->serverConfig->isCookieHttpOnly()
            );
        } catch (OAuthServerException $e) {
            if (!in_array($e->getHttpStatusCode(), [400, 401], true)) {
                $this->logger->critical($e->getMessage(), $e->getTrace());
            }

            return $e->generateHttpResponse($response);
        }
    }

    public function logout(): ResponseInterface
    {
        return OAuthHelper::clearTokenFromCookie(new Response(), $this->serverConfig->getCookieName());
    }

    public function getServer(): LeagueAuthorizationServer
    {
        $this->initialize();
        return $this->server;
    }

    private function initialize(): void
    {
        if ($this->initialized) {
            return;
        }

        $this->server->setDefaultScope($this->serverConfig->getDefaultScope());

        // add auth code grant
        $authCode = new AuthCodeGrant(
            $this->authCodeRepository,
            $this->refreshTokenRepository,
            $this->serverConfig->getAuthCodeLifetime()
        );
        $this->server->enableGrantType($authCode, $this->serverConfig->getAccessTokenLifetime());

        // add password grant
        $grantPassword = new PasswordGrant($this->userRepository, $this->refreshTokenRepository);
        $grantPassword->setRefreshTokenTTL($this->serverConfig->getRefreshTokenLifetime());
        $this->server->enableGrantType($grantPassword, $this->serverConfig->getAccessTokenLifetime());

        // add refresh token grant
        $grantRefreshToken = new RefreshTokenGrant($this->refreshTokenRepository);
        $grantRefreshToken->setRefreshTokenTTL($this->serverConfig->getRefreshTokenLifetime());
        $this->server->enableGrantType($grantRefreshToken, $this->serverConfig->getAccessTokenLifetime());

        $this->dispatcher->dispatch(new AuthorizationServerInitializationEvent($this->server));

        $this->initialized = true;
    }
}
