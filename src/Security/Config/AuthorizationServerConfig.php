<?php

declare(strict_types=1);

namespace UXF\OAuth2\Security\Config;

use DateInterval;

class AuthorizationServerConfig
{
    /** @var string */
    private $redirectLoginPath;

    /** @var string */
    private $redirectApprovePath;

    /** @var string */
    private $authCodeLifetime;

    /** @var string */
    private $accessTokenLifetime;

    /** @var string */
    private $refreshTokenLifetime;

    /** @var string */
    private $defaultScope;

    /** @var string[] */
    private $clientsWhitelist;

    /** @var string */
    private $cookieName;

    /** @var bool|null */
    private $cookieSecured;

    /** @var bool */
    private $cookieHttpOnly;

    /**
     * @param string[] $clientsWhitelist
     */
    public function __construct(
        string $redirectLoginPath,
        string $redirectApprovePath,
        string $authCodeLifetime,
        string $accessTokenLifetime,
        string $refreshTokenLifetime,
        string $defaultScope,
        array $clientsWhitelist,
        string $cookieName,
        ?bool $cookieSecured,
        bool $cookieHttpOnly
    ) {
        $this->redirectLoginPath = $redirectLoginPath;
        $this->redirectApprovePath = $redirectApprovePath;
        $this->authCodeLifetime = $authCodeLifetime;
        $this->accessTokenLifetime = $accessTokenLifetime;
        $this->refreshTokenLifetime = $refreshTokenLifetime;
        $this->defaultScope = $defaultScope;
        $this->clientsWhitelist = $clientsWhitelist;
        $this->cookieSecured = $cookieSecured;
        $this->cookieHttpOnly = $cookieHttpOnly;
        $this->cookieName = $cookieName;
    }

    public function getRedirectLoginPath(): string
    {
        return $this->redirectLoginPath;
    }

    public function getRedirectApprovePath(): string
    {
        return $this->redirectApprovePath;
    }

    public function getAuthCodeLifetime(): DateInterval
    {
        return new DateInterval($this->authCodeLifetime);
    }

    public function getAccessTokenLifetime(): DateInterval
    {
        return new DateInterval($this->accessTokenLifetime);
    }

    public function getRefreshTokenLifetime(): DateInterval
    {
        return new DateInterval($this->refreshTokenLifetime);
    }

    public function getDefaultScope(): string
    {
        return $this->defaultScope;
    }

    /**
     * @return string[]
     */
    public function getClientsWhitelist(): array
    {
        return $this->clientsWhitelist;
    }

    public function isCookieSecured(): ?bool
    {
        return $this->cookieSecured;
    }

    public function isCookieHttpOnly(): bool
    {
        return $this->cookieHttpOnly;
    }

    public function getCookieName(): string
    {
        return $this->cookieName;
    }
}
