<?php

declare(strict_types=1);

namespace UXF\OAuth2\Security\Config;

class DefaultClientConfig
{
    /** @var string */
    private $clientId;

    /** @var string */
    private $clientSecret;

    /** @var string */
    private $scope;

    public function __construct(string $clientId, string $clientSecret, string $scope)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->scope = $scope;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public function getScope(): string
    {
        return $this->scope;
    }
}
