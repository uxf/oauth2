<?php

declare(strict_types=1);

namespace UXF\OAuth2\Security;

use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use UXF\Core\Http\Factory\ResponseFactory;
use UXF\OAuth2\Security\Config\AuthorizationServerConfig;

class UxfAuthenticator extends AbstractAuthenticator
{
    /** @var string */
    private $authorizationHeader;

    /** @var ResourceServer */
    private $resourceServer;

    /** @var ResponseFactory */
    private $responseFactory;

    /** @var ServerRequestFactoryInterface */
    private $serverRequestFactory;

    /** @var AuthorizationServerConfig */
    private $authorizationServerConfig;

    public function __construct(
        string $authorizationHeader,
        ResourceServer $resourceServer,
        ResponseFactory $responseFactory,
        ServerRequestFactoryInterface $serverRequestFactory,
        AuthorizationServerConfig $authorizationServerConfig
    ) {
        $this->authorizationHeader = $authorizationHeader;
        $this->resourceServer = $resourceServer;
        $this->responseFactory = $responseFactory;
        $this->serverRequestFactory = $serverRequestFactory;
        $this->authorizationServerConfig = $authorizationServerConfig;
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has($this->authorizationHeader)
            || $request->cookies->has($this->authorizationServerConfig->getCookieName());
    }

    public function authenticate(Request $request): Passport
    {
        $token = $request->headers->has($this->authorizationHeader)
                ? $request->headers->get($this->authorizationHeader)
                : $request->cookies->get($this->authorizationServerConfig->getCookieName());

        try {
            // $token = urldecode($credentials['token']); TODO inspect
            $psrRequest = $this->serverRequestFactory->createServerRequest('POST', '')
                ->withHeader('Authorization', (string) $token);

            $validatedRequest = $this->resourceServer->validateAuthenticatedRequest($psrRequest);

            $userId = (string) $validatedRequest->getAttribute('oauth_user_id');

            return new SelfValidatingPassport(new UserBadge($userId));
        } catch (OAuthServerException) {
            throw new AuthenticationException();
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return $this->responseFactory->unauthorized();
    }
}
