<?php

declare(strict_types=1);

namespace UXF\OAuth2;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use UXF\OAuth2\DependencyInjection\UXFOAuth2Extension;

class UXFOAuth2Bundle extends Bundle
{
    public function getContainerExtension(): ExtensionInterface
    {
        return new UXFOAuth2Extension();
    }
}
