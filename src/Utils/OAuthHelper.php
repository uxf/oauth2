<?php

declare(strict_types=1);

namespace UXF\OAuth2\Utils;

use Nette\Utils\Json;
use Psr\Http\Message\ResponseInterface;
use Safe\DateTime;
use Symfony\Component\HttpFoundation\Cookie;

class OAuthHelper
{
    public static function saveTokenToCookie(
        ResponseInterface $response,
        string $cookieName,
        ?bool $secure,
        bool $httpOnly
    ): ResponseInterface {
        $responseData = Json::decode((string) $response->getBody());
        $response->getBody()->rewind(); //reset

        $now = new DateTime();

        $cookie = Cookie::create(
            $cookieName,
            "{$responseData->token_type} {$responseData->access_token}",
            $now->modify("+{$responseData->expires_in} seconds"),
            '/',
            null,
            $secure,
            $httpOnly,
            true
        );

        return $response->withHeader('Set-Cookie', (string) $cookie);
    }

    public static function clearTokenFromCookie(ResponseInterface $response, string $cookieName): ResponseInterface
    {
        return $response->withHeader(
            'Set-Cookie',
            (string) Cookie::create($cookieName, '', new DateTime())
        );
    }
}
