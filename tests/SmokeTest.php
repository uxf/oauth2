<?php

declare(strict_types=1);

namespace UXF\OAuth2Tests;

use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Token\Plain;
use League\OAuth2\Server\AuthorizationServer as AuthServer;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Nette\Utils\Json;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7\Stream;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Safe\DateTimeImmutable;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use UXF\OAuth2\Entity\AccessToken;
use UXF\OAuth2\Entity\Client;
use UXF\OAuth2\Entity\Scope;
use UXF\OAuth2\Security\AuthorizationServer;
use UXF\OAuth2\Security\Config\AuthorizationServerConfig;
use UXF\OAuth2\Security\Config\DefaultClientConfig;

class SmokeTest extends TestCase
{
    public function testAuthorizationServer(): void
    {
        $accessToken = new AccessToken(
            new Client('uxf', 'uxf', 'uxf'),
            [new Scope('uxf_scope', '')],
            '1'
        );
        $accessToken->setIdentifier('id');
        $accessToken->setExpiryDateTime(new DateTimeImmutable());
        $accessToken->setPrivateKey(new CryptKey(__DIR__ . '/keys/private.key'));

        /** @var AuthServer $authServer */
        $authServer = $this->createMock(AuthServer::class);

        if ($authServer instanceof MockObject) {
            $authServer
                ->method('respondToAccessTokenRequest')
                ->willReturn((new Response())->withBody(Stream::create("{\"access_token\": \"$accessToken\"}")));
        }

        /** @var UserRepositoryInterface $userRepository */
        $userRepository = $this->createMock(UserRepositoryInterface::class);

        /** @var AuthCodeRepositoryInterface $authCodeRepository */
        $authCodeRepository = $this->createMock(AuthCodeRepositoryInterface::class);

        /** @var RefreshTokenRepositoryInterface $refreshTokenRepository */
        $refreshTokenRepository = $this->createMock(RefreshTokenRepositoryInterface::class);

        /** @var TokenStorageInterface $tokenStorage */
        $tokenStorage = $this->createMock(TokenStorageInterface::class);

        $authorizationServer = new AuthorizationServer(
            $authServer,
            $userRepository,
            $authCodeRepository,
            $refreshTokenRepository,
            $tokenStorage,
            new NullLogger(),
            new EventDispatcher(),
            new AuthorizationServerConfig('/', '/', 'PT1H', 'PT1H', 'PT1H', '', [], 'Auth-Token', false, true),
            new DefaultClientConfig('', '', '')
        );

        $requestData = [
            'username' => 'root@uxf.cz',
            'password' => 'root',
            'grant_type' => 'password',
            'client_id' => 'uxf',
            'client_secret' => 'secret',
            'scope' => 'uxf_scope',
        ];

        $request = new ServerRequest(
            'POST',
            '/api/login',
            ['Content-Type' => 'application/json'],
            Json::encode($requestData)
        );

        $response = $authorizationServer->token($request);
        self::assertEquals(200, $response->getStatusCode());

        // send authenticated request
        $response->getBody()->rewind();
        $responseData = Json::decode($response->getBody()->getContents(), Json::FORCE_ARRAY);
        $token = $responseData['access_token'];

        /** @var Plain $jwtToken */
        $jwtToken = (new Parser(new JoseEncoder()))->parse($token);

        self::assertEquals(['uxf_scope'], $jwtToken->claims()->get('scopes'));
    }
}
